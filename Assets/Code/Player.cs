using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private Transform model;
    [SerializeField]
    private Bullet bulletPrefab;
    [SerializeField]
    private Transform bulletOrigin;

    private Direction direction;

    private Direction Dir
    {
        get { return direction; }
        set { if (value != direction) ChangeDirection(value);
            direction = value;
        }
    }
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 moveDirection = new Vector3(horizontal,0,vertical);
        CheckDirection(moveDirection);
        transform.Translate(moveDirection.normalized * speed * Time.deltaTime);
        TryFire();
    }

private void CheckDirection(Vector3 moveDirection)
    {
        if (moveDirection.x > 0) Dir = Direction.Right;
        if (moveDirection.x < 0) Dir = Direction.Left;
        if (moveDirection.z > 0) Dir = Direction.Up;
        if (moveDirection.z < 0) Dir = Direction.Down;
    }

    private void ChangeDirection(Direction d)
    {
        switch (d)
        {
            case Direction.Right: model.rotation = Quaternion.Euler(0, 90, 0); break;
            case Direction.Left: model.rotation = Quaternion.Euler(0, -90, 0); break;
            case Direction.Up: model.rotation = Quaternion.Euler(0, 0, 0); break;
            case Direction.Down: model.rotation = Quaternion.Euler(0, 180, 0); break;
        }

    }
    private void TryFire()
    {
        if(Input.GetButtonDown("Fire1"))
        {
            var bullet = Instantiate(bulletPrefab, bulletOrigin.position, Quaternion.identity);
            bullet.Setup(model.forward);
        }
    }
}

public enum Direction
{
    Up,
    Down,
    Left,
    Right
}