using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Candy : MonoBehaviour
{
    public int pointsToAdd = 1;
  
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            GameController.instance.AddPoint(pointsToAdd);
            Destroy(gameObject);
        }
    }
}
