using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    [SerializeField] private GameData data;

    private static List<Vector2> rooms;

    private static bool bossSpawned;

    private static float timer = 0f;
    private void Awake()
    {
        rooms = new List<Vector2>();
        timer = 0f;
        bossSpawned = false;
    }

    private void Update()
    {
        if (timer < 0.5f)
        {
            timer += Time.deltaTime;
        }
        else
        {
            if (!bossSpawned) SpawnBoss();
        }
    }

    public static bool HasRoomAt(Vector3 position)
    {
        var pos = new Vector2((int)position.x, (int)position.z);
        if(rooms.Contains(pos))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void AssignRoom(Vector3 position)
    {
        var pos = new Vector2((int)position.x, (int)position.z);
        timer = 0f;
        if(!rooms.Contains(pos))
        {
            rooms.Add(pos);
        }
    }

    private void SpawnBoss()
    {
        bossSpawned = true;
        var i = rooms.Count - 1;
        var position = new Vector3(rooms[i].x, 1f, rooms[i].y);
        Instantiate(data.Boss, position, Quaternion.identity);
    }
}
