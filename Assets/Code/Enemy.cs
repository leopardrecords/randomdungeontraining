using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    public float leftMargin;
    public float rightMargin;

    private float currentPos = 0;
    private int direction = 1;
    private void Update()
    {
        Move();
    }

    private void Move()
    {
        Vector3 movement = new Vector3(speed * direction * Time.deltaTime, 0, 0);

        if(direction == 1)
        {
            if(currentPos < rightMargin)
            {
                currentPos += Time.deltaTime * speed * direction;
            }
            else if(currentPos >= rightMargin)
            {
                direction = -1;
            }
        }

        if(direction == -1)
        {
            if (currentPos > leftMargin)
            {
                currentPos += Time.deltaTime * speed * direction;
            }
            else if (currentPos <= leftMargin)
            {
                direction = 1;
            }
        }

        transform.Translate(movement);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.CompareTag("Player"))
        {
            GameController.instance.Restart();
        }
    }
}
