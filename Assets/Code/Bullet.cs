using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed = 7f;
    [SerializeField] private float lifeTime = 1f;

    private Vector3 direction = Vector3.zero;
    private bool isSet = false;
    private float timer = 0f;

    private void Update()
    {
        if(isSet)
        {
            timer += Time.deltaTime;
            transform.Translate(direction * speed * Time.deltaTime);
            if(timer >= lifeTime)
            {
                Destroy(gameObject);
            }
        }
    }

    public void Setup(Vector3 direction)
    {
        this.direction = direction;
        isSet = true;
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
}
