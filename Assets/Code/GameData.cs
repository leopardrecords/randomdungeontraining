using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData",menuName ="Data/GameData")]
public class GameData : ScriptableObject
{
    [SerializeField] private GameObject[] upRooms;
    [SerializeField] private GameObject[] downRooms;
    [SerializeField] private GameObject[] leftRooms;
    [SerializeField] private GameObject[] rightRooms;
    [SerializeField] private GameObject boss;


    public GameObject GetRandomRoom(OpeningDirection direction)
    {
        switch (direction)
        {
            case OpeningDirection.NeedUpDoor:
                return upRooms[Random.Range(0, upRooms.Length)];
            case OpeningDirection.NeedDownDoor:
                return downRooms[Random.Range(0, downRooms.Length)];
            case OpeningDirection.NeedLeftDoor:
                return leftRooms[Random.Range(0, leftRooms.Length)];
            case OpeningDirection.NeedRightDoor:
                return rightRooms[Random.Range(0, rightRooms.Length)];
            default: return null;
        }
    }
    public GameObject Boss => boss;
}
