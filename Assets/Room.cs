using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    private CameraController cameraController;
    private Vector3 position;

    private void Start()
    {
        position = transform.position;
        cameraController = FindObjectOfType<CameraController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            cameraController.SetPosition(position);
        }
    }
}
