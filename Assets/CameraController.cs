using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private float smooth = 4f;
    private Vector3 position;

    private void Start()
    {
        position = transform.position;
    }

    public void SetPosition(Vector3 pos)
    {
        position = new Vector3(pos.x, position.y, pos.z);
    }

    private void Update()
    {
        if (Vector3.Distance(transform.position, position) > 0.1f)
        {
            transform.position = Vector3.Lerp(transform.position, position, smooth * Time.deltaTime);
        }
    }

}
